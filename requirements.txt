##### basic requirements #####

fasteners
future
mock
python-gettext
requests
urllib3


##### testing libraries #####

coverage
paramiko
pexpect
psutil
pycodestyle
pydevd
pylint
pytest
pytest-cov
pytest-runner
sphinx
tox


##### backend libraries #####

# azure
# b2sdk
# boto
# boto3
# dropbox==6.9.0
# gdata
# jottalib
# mediafire
# pydrive
# pyrax
# python-cloudfiles
# python-swiftclient
# requests_oauthlib
